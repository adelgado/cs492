function openNav() {
    document.getElementById("mySidebar").style.width = "20%";
    document.getElementById("main").style.marginLeft = "20%";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("headerBackground").style.fontSize = "70%";
    } else {
        document.getElementById("headerBackground").style.fontSize = "150%";
    }
}

/* adapted from https://www.sitepoint.com/simple-javascript-quiz/ */

var myQuestions = [
    {
        question: "1. 1 in ____ computer science graduates in Europe were women?",
        answers: {
            a: '3',
            b: '5',
            c: '7'
        },
        correctAnswer: 'b'
    },
    {
        question: "2. Which scandanavian country has the last share of female STEM graduates?",
        answers: {
            a: 'Norway',
            b: 'Denmark',
            c: 'Sweden'
        },
        correctAnswer: 'a'
    },
    {
        question: "3. What is the percentage rise of girls take IT courses in higher education?",
        answers: {
            a: '2',
            b: '6',
            c: '9'
        },
        correctAnswer: 'c'
    },
    {
        question: "4. How many priorities are highlighted in the policy document called “Digital agenda for Norge”?",
        answers: {
            a: '5',
            b: '2',
            c: '3'
        },
        correctAnswer: 'a'
    },
    {
        question: "5. When was the Nordic Female Entrepeneurship initiative founded?",
        answers: {
            a: '2018',
            b: '2019',
            c: '2020'
        },
        correctAnswer: 'a'
    },
    {
        question: "6. Guess which Norwegian word means equality:",
        answers: {
            a: 'Likestilling',
            b: 'Høre',
            c: 'Unnskyldning'
        },
        correctAnswer: 'a'
    }
];

var quizContainer = document.getElementById('quiz');
var resultsContainer = document.getElementById('results');
var submitButton = document.getElementById('submit');

function generateQuiz(questions, quizContainer, resultsContainer, submitButton){
    

	function showQuestions(questions, quizContainer){
        // we'll need a place to store the output and the answer choices
        var output = [];
        var answers;
    
        // for each question...
        for(var i=0; i<questions.length; i++){
            
            // first reset the list of answers
            answers = [];
    
            // for each available answer to this question...
            for(letter in questions[i].answers){
    
                // ...add an html radio button
                answers.push(
                    '<label>'
                        + '<input type="radio" name="question'+i+'" value="'+letter+'">'
                        + letter + ': '
                        + questions[i].answers[letter]
                    + '</label>'
                );
            }
    
            // add this question and its answers to the output
            output.push(
                '<div class="question">' + questions[i].question + '</div>'
                + '<div class="answers">' + answers.join('') + '</div>'
            );
        }
    
        // finally combine our output list into one string of html and put it on the page
        quizContainer.innerHTML = output.join('');
    }

	function showResults(questions, quizContainer, resultsContainer){
	
        // gather answer containers from our quiz
        var answerContainers = quizContainer.querySelectorAll('.answers');
        
        // keep track of user's answers
        var userAnswer = '';
        var numCorrect = 0;
        
        // for each question...
        for(var i=0; i<questions.length; i++){
    
            // find selected answer
            userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;
            
            // if answer is correct
            if(userAnswer===questions[i].correctAnswer){
                // add to the number of correct answers
                numCorrect++;
                
                // color the answers green
                answerContainers[i].style.color = 'lightgreen';
            }
            // if answer is wrong or blank
            else{
                // color the answers red
                answerContainers[i].style.color = 'red';
            }
        }
    
        // show number of correct answers out of total
        resultsContainer.innerHTML = numCorrect + ' out of ' + questions.length;
    }

	// show the questions
	showQuestions(questions, quizContainer);

	// when user clicks submit, show results
	submitButton.onclick = function(){
		showResults(questions, quizContainer, resultsContainer);
	}
}
generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);