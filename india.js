function openNav() {
    document.getElementById("mySidebar").style.width = "20%";
    document.getElementById("main").style.marginLeft = "20%";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

function scrollFunction() {
    if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
        document.getElementById("headerBackground").style.fontSize = "70%";
    } else {
        document.getElementById("headerBackground").style.fontSize = "150%";
    }
}

/* adapted from https://www.sitepoint.com/simple-javascript-quiz/ */

var myQuestions = [
    {
        question: "1. Gender disparity exists in Indian tech industries for multiple reasons. How many reasons have been highlighted in the text?",
        answers: {
            a: '6',
            b: '10',
            c: '12',
            d: '8'
        },
        correctAnswer: 'a'
    },
    {
        question: "2. Which year was Section 377 decriminalized in India?",
        answers: {
            a: '2012',
            b: '2015',
            c: '2018',
            d: '2022'
        },
        correctAnswer: 'c'
    },
    {
        question: "3. What percentage of Women make the workforce according to Mamta Sharma?",
        answers: {
            a: '10%',
            b: '5%',
            c: '45%',
            d: '31%'
        },
        correctAnswer: 'd'
    },
    {
        question: "4. By what percentage has the number of women in leaderships roles reduced by",
        answers: {
            a: '10%',
            b: '1%',
            c: '-5%',
            d: '3%'
        },
        correctAnswer: 'd'
    },
    {
        question: "5. How much funding has the Indian government given to companies created by Women in Tech?",
        answers: {
            a: '$25 Million',
            b: '$30 Million',
            c: '$35 Million',
            d: '$40 Million'
        },
        correctAnswer: 'c'
    },
    {
        question: "6. Why are LGBTQ+ communities not have alot of relevant data?",
        answers: {
            a: 'Inaccuracies in reports that captures this data',
            b: 'Many members of LGBTQ community in India are closeted because of prejudice and discrimination',
            c: 'This is not something organizations are interested in',
            d: 'People give little to no heed to such information'
        },
        correctAnswer: 'b'
    }
];

var quizContainer = document.getElementById('quiz');
var resultsContainer = document.getElementById('results');
var submitButton = document.getElementById('submit');

function generateQuiz(questions, quizContainer, resultsContainer, submitButton){
    

	function showQuestions(questions, quizContainer){
        // we'll need a place to store the output and the answer choices
        var output = [];
        var answers;
    
        // for each question...
        for(var i=0; i<questions.length; i++){
            
            // first reset the list of answers
            answers = [];
    
            // for each available answer to this question...
            for(letter in questions[i].answers){
    
                // ...add an html radio button
                answers.push(
                    '<label>'
                        + '<input type="radio" name="question'+i+'" value="'+letter+'">'
                        + letter + ': '
                        + questions[i].answers[letter]
                    + '</label>'
                );
            }
    
            // add this question and its answers to the output
            output.push(
                '<div class="question">' + questions[i].question + '</div>'
                + '<div class="answers">' + answers.join('') + '</div>'
            );
        }
    
        // finally combine our output list into one string of html and put it on the page
        quizContainer.innerHTML = output.join('');
    }

	function showResults(questions, quizContainer, resultsContainer){
	
        // gather answer containers from our quiz
        var answerContainers = quizContainer.querySelectorAll('.answers');
        
        // keep track of user's answers
        var userAnswer = '';
        var numCorrect = 0;
        
        // for each question...
        for(var i=0; i<questions.length; i++){
    
            // find selected answer
            userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;
            
            // if answer is correct
            if(userAnswer===questions[i].correctAnswer){
                // add to the number of correct answers
                numCorrect++;
                
                // color the answers green
                answerContainers[i].style.color = 'lightgreen';
            }
            // if answer is wrong or blank
            else{
                // color the answers red
                answerContainers[i].style.color = 'red';
            }
        }
    
        // show number of correct answers out of total
        resultsContainer.innerHTML = numCorrect + ' out of ' + questions.length;
    }

	// show the questions
	showQuestions(questions, quizContainer);

	// when user clicks submit, show results
	submitButton.onclick = function(){
		showResults(questions, quizContainer, resultsContainer);
	}
}
generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);